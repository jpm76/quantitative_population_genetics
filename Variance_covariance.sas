/*input data*/

FILENAME REFFILE "Zscore_DFA_phenotype_Drydata0603.csv" TERMSTR=CR;

PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=phenodata_Zscore_Wet;
	GETNAMES=YES;
RUN;

/*creat dummy variable*/
/*Traits list--Std_Height	Std_Diameter	Std_Leaf_num	Std_Leaf_length	Std_Leaf_width	Std_FW_A	Std_DW_A	Std_WC_A	Std_Square_Root_LWC	Std_Square_Root_Volume	Std_Square_Root_Leaf_shape	Std_Square_Root_Leaf_area	Std_Square_Root_Leaf_packing	Std_Square_Root_Freshwt	Std_Square_Root_Drywt*/

/*Height-Diameter*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Diameter=y));
    if Std_Height=. then Trait=1 ;
    if Std_Diameter=. then Trait=2 ;
    drop Std_Height Std_Diameter;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Height-Leafnum*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_num=y));
    if Std_Height=. then Trait=1 ;
    if Std_Leaf_num=. then Trait=3 ;
    drop Std_Height Std_Leaf_num;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;



/*Height-Leaflength*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_length=y));
    if Std_Height=. then Trait=1 ;
    if Std_Leaf_length=. then Trait=4 ;
    drop Std_Height Std_Leaf_length;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    

/*Height-Leafwidth*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_width=y));
    if Std_Height=. then Trait=1 ;
    if Std_Leaf_width=. then Trait=5 ;
    drop Std_Height Std_Leaf_width;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
/*Height-FW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_FW_A=y));
    if Std_Height=. then Trait=1 ;
    if Std_FW_A=. then Trait=6 ;
    drop Std_Height Std_FW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    

/*Height-DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_Height=. then Trait=1 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_Height Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*Height-WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_Height=. then Trait=1 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_Height Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
     
    
/*Height-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_Height Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    


   
/*Height-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Height Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Height-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Height Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Height-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Height Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Height-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Height Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;

/*Height-FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Height Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Height-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Height=. then Trait=1 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Height Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
/*Height-END*/
 
 
      
/*Diameter-start*/

/*Diameter-Leafnum*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_num=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Leaf_num=. then Trait=3 ;
    drop Std_Diameter Std_Leaf_num;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;



/*Diameter-Leaflength*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_length=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Leaf_length=. then Trait=4 ;
    drop Std_Diameter Std_Leaf_length;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    

/*Diameter-Leafwidth*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_width=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Leaf_width=. then Trait=5 ;
    drop Std_Diameter Std_Leaf_width;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
/*Diameter-FW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_FW_A=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_FW_A=. then Trait=6 ;
    drop Std_Diameter Std_FW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    

/*Diameter-DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_Diameter Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*Diameter-WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_Diameter Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
/*Diameter-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_Diameter Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;   


    
/*Diameter-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Diameter Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Diameter-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Height=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Height=. then Trait=2 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Height Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Diameter-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Diameter Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Diameter-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Diameter Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Diameter Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Diameter-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Diameter=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Diameter=. then Trait=2 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Diameter Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;      
      
/*Diameter-END*/


/**Leafnum--start**/


/*Leafnum-Leaflength*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_length=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Leaf_length=. then Trait=4 ;
    drop Std_Leaf_num Std_Leaf_length;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    

/*Leafnum-Leafwidth*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_width=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Leaf_width=. then Trait=5 ;
    drop Std_Leaf_num Std_Leaf_width;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
/*Leafnum-FW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_FW_A=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_FW_A=. then Trait=6 ;
    drop Std_Leaf_num Std_FW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    

/*Leafnum-DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_Leaf_num Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*Leafnum-WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_Leaf_num Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    
/*Leafnum-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_Leaf_num Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    




    
/*Leafnum-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Leaf_num Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leafnum-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Leaf_num Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leafnum-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Leaf_num Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Leafnum-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Leaf_num Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Leafnum-FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Leaf_num Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leafnum-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_num=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Leaf_num=. then Trait=3 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Leaf_num Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;       
/*Leafnum--End*/

/*Leaflength--start*/


/*Leaflength-Leafwidth*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Leaf_width=y));
    if Std_Leaf_length=. then Trait=3 ;
    if Std_Leaf_width=. then Trait=5 ;
    drop Std_Leaf_length Std_Leaf_width;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
/*Leaflength-FW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_FW_A=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_FW_A=. then Trait=6 ;
    drop Std_Leaf_length Std_FW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    

/*Leaflength-DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_Leaf_length Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*Leaflength-WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_Leaf_length Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
/*Leaf_length-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_Leaf_length Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;     



    
/*Leaflength-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Leaf_length Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leaflength-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Leaf_length Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leaflength-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Leaf_length Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Leaflength-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Leaf_length Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Leaflength-FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Leaf_length Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leaflength-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_length=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Leaf_length=. then Trait=4 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Leaf_length Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 

/*Leaflength--End*/

/*Leafwidth--start*/    
      
/*Leafwidth-FW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_FW_A=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_FW_A=. then Trait=6 ;
    drop Std_Leaf_width Std_FW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    

/*Leafwidth-DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_Leaf_width Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*Leafwidth-WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_Leaf_width Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    
/*Leaf_width-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_Leaf_width Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;     



    
/*Leafwidth-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Leaf_width Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leafwidth-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Leaf_width Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Leafwidth-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Leaf_width Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Leafwidth-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Leaf_width Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Leafwidth-FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Leaf_width Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leafwidth-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Leaf_width=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Leaf_width=. then Trait=5 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Leaf_width Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;  
      
/*Leafwidth--END*/


/*FW_A---start*/
/*FW_A--DW_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_DW_A=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_DW_A=. then Trait=7 ;
    drop Std_FW_A Std_DW_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    

/*FW_A--WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_FW_A Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
    
/*FW_A-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_FW_A Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;   


    
/*FW_A--Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_FW_A Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*FW_A--Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_FW_A Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*FW_A--Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_FW_A Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*FW_A--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_FW_A Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*FW_A--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_FW_A Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*FW_A--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_FW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_FW_A=. then Trait=6 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_FW_A Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 

/*FW_A---END*/

/*DW_A---START*/


/*DW_A--WC_A*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_WC_A=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_WC_A=. then Trait=8 ;
    drop Std_DW_A Std_WC_A;
    run;
    

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;    
    
    
/*DW_A-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_DW_A Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 


    
/*DW_A--Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_DW_A Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*DW_A--Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_DW_A Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*DW_A--Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_DW_A Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*DW_A--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_DW_A Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*DW_A--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_DW_A Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*DW_A--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_DW_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_DW_A=. then Trait=7 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_DW_A Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;       
/*DW_A---END*/

/*WC_A--START*/

/*WC_A-LWC*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_LWC=. then Trait=9 ;
    drop Std_WC_A Std_Square_Root_LWC;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 

    
/*WC_A--Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_WC_A Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*WC_A--Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_WC_A Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*WC_A--Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_WC_A Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*WC_A--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_WC_A Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*WC_A--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_WC_A Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*WC_A--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_WC_A=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_WC_A=. then Trait=8 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_WC_A Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;       
/*WC_A---END*/

/*LWC--START*/

    
/*LWC-Volume*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Volume=. then Trait=10 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Volume;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*LWC-Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*LWC-Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*LWC-Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;

/*LWC-FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*LWC-DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_LWC =y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_LWC =. then Trait=9 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_LWC  Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 


/*LWC--STOP*/

/*Volume--start*/

/*Volume--Leafshape*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y));
    if Std_Square_Root_Volume=. then Trait=10 ;
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    drop Std_Square_Root_Volume Std_Square_Root_Leaf_shape;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;




/*Volume--Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Square_Root_Volume=. then Trait=10 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Square_Root_Volume Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Volume--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Square_Root_Volume=. then Trait=10 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Square_Root_Volume Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Volume--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Square_Root_Volume=. then Trait=10 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Square_Root_Volume Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Volume--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Volume=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_Volume=. then Trait=10 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_Volume Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 
/*Volume---END*/

/*Leafshape--start*/


/*Leafshape--Leaf area*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y));
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    drop Std_Square_Root_Leaf_shape Std_Square_Root_Leaf_area;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
         

/*Leafshape--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Square_Root_Leaf_shape Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Leafshape--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Square_Root_Leaf_shape Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leafshape--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_shape=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_Leaf_shape=. then Trait=11 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_Leaf_shape Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;       
/*Leafshpe---END*/

/*Leafarea--start*/


/*Leafarea--Leaf packing*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y));
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    drop Std_Square_Root_Leaf_area Std_Square_Root_Leaf_packing;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;


/*Leafarea--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Square_Root_Leaf_area Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leafarea--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_area=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_Leaf_area=. then Trait=12 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_Leaf_area Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;       
 
/*Leafarea---End*/

/*Leafpacking--start*/ 
/*Leafpacking--FreshWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y));
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    drop Std_Square_Root_Leaf_packing Std_Square_Root_Freshwt;
    run;


/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run;
    
  
/*Leafpacking--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Leaf_packing=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_Leaf_packing=. then Trait=13 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_Leaf_packing Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 
/*Leafpacking--END*/

/*FreshWt--start*/

/*FreshWt--DryWt*/
data A;
    set phenodata_Zscore_Wet(rename=(Std_Square_Root_Freshwt=y))
    phenodata_Zscore_Wet(rename=(Std_Square_Root_Drywt=y));
    if Std_Square_Root_Freshwt=. then Trait=14 ;
    if Std_Square_Root_Drywt=. then Trait=15 ;
    drop Std_Square_Root_Freshwt Std_Square_Root_Drywt;
    run;
 

/*Fix mixed model--get genetic covariance*/
/*It works*/
proc mixed DATA=A covtest asycov scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp asycov=_cov ;                 
      run;


/*calculate SE*/
proc iml ;

use _varcomp;
read all var {Estimate} where(Subject="Genotype") into _varcomp; 
close _varcomp;

use _cov; 
read all var {CovP1 CovP2 CovP3} into _cov;
close _cov;

/*Genetic correlation*/
R = _varcomp[2,1]/sqrt(_varcomp[1,1]*_varcomp[3,1]);


/* Standard error of genetic correlation */
a =_cov[1,1]/(4*(_varcomp[1,1])**2) ; 
ab=_cov[2,2]/((_varcomp[2,1])**2) ; 
b =_cov[3,3]/(4*(_varcomp[3,1])**2) ;


c1=(2*_cov[1,3])/ (4*(_varcomp[1,1]*_varcomp[3,1])); 
c2=(2*_cov[1,2])/ (2*(_varcomp[1,1]*_varcomp[2,1])); 
c3=(2*_cov[2,3])/ (2*(_varcomp[2,1]*_varcomp[3,1]));

/** Variance of genetic correlation**/
var_r=(r*r)*(a+b+ab+c1-c2-c3);
/** Standard error of genetic correlation**/
SE_r =sqrt(var_r) ;
print r var_r SE_r ;
run; 

/*reduced model--control covariance to 0*/
proc mixed DATA=A covtest scoring=1; 
      class Trait Genotype Block;
      model y = Trait/ddfm = SATTERTHWAITE;
      random Trait/type=un(1) subject=genotype;
      random Trait/type=VC subject=Block group=Trait;
      ods output covparms=_varcomp;                 
      run; 
/*FreshWt--END*/         