# oldest script
# for file in /work/jpm76/fst/parsedfst/*.fst.txt
# do
# echo "parsing file $file"
# sed 'id' $file > $$.tmp && mv $$.tmp $file
# sed 's/^/chr/' $file > $$.tmp && mv $$.tmp $file
# awk '{print $1"\t"$2"\t"$2"\t"$3}' $file > $file.bed
# done

# for file in /work/jpm76/fst/parsedfst/*.fst.txt
# do
# echo "parsing file $file"
# head -100 $file > $file.test
# done

# for file in /work/jpm76/fst/parsedfst/*.fst.txt.test
# do
# echo "parsing file $file"
# sed '1d' $file > $$.tmp && mv $$.tmp $file
# sed 's/^/chr/' $file > $$.tmp && mv $$.tmp $file
# awk '{print $1"\t"$2"\t"$2"\t"$3"\t"FILENAME}' $file > $file.bed #adds filename to the
# sed -i 's/\/work\/jpm76\/fst\/parsedfst\///' $file.bed # remove the path
# sed -i 's/'.'fst'.'txt'.'test//g' $file.bed # remove the other txts... -i means replace the identity of the input... should use it more often
# done

#copy original file 
cp /work/jpm76/fst/*.fst.txt ./parsedfst/

# parse each file
for file in /work/jpm76/fst/parsedfst/*.fst.txt
do
echo "parsing file $file"
sed '1d' $file > $$.tmp && mv $$.tmp $file
sed 's/^/chr/' $file > $$.tmp && mv $$.tmp $file
awk '{print $1"\t"$2"\t"$2"\t"$3"\t"FILENAME}' $file > $file.bed #adds filename to the 
sed -i 's/\/work\/jpm76\/fst\/parsedfst\///' $file.bed # remove the path
sed -i 's/'.'fst'.'txt//g' $file.bed # remove the other txts... -i means replace the identity of the input... should use it more often
done

# checks each file
for file in /work/jpm76/fst/parsedfst/*.fst.txt.bed
do
echo "parsing file $file"
tail $file
echo "*****************************************************"
done


# for file in /work/jpm76/fst/parsedfst/*.fst.txt.bed
# do
# echo "parsing file $file"
# grep -w "chr7" $file
# echo "*****************************************************"
# done



# for file in /work/jpm76/fst/parsedfst/*.fst.txt.bed
# do
# echo "parsing file $file"
# grep -w "chr7" $file >> chr7.txt
# echo "*****************************************************"
# done





# for file in /work/jpm76/fst/parsedfst/*.fst.txt.test.bed
# do
# 	cat $file >> superFST.bed
# done


# combine the parsed files
for file in /work/jpm76/fst/parsedfst/*.fst.txt.bed
do
	cat $file >> superFST.bed
done





# sorts superFST.file
sort -k1,1 -k2,2n superFST.bed > superFST_sorted.bed


# removes the weird chrchr7 txt
sed -i 's/chrchr7/chr7/g' superFST_sorted.bed

# resort the sorted
sort -k1,1 -k2,2n superFST_sorted.bed > superFST_sorted2.bed



#paste <(cut -f 1,2,3,5  superFST_sorted.bed) <(cut -f4  superFST_sorted.bed) > superFST_sorted_arranged.bed




#remove duplicated lines

awk '! a[$0]++' superFST_sorted2.bed > unique_superFST_sorted2.bed


# get the mean, min, max, median

# bedtools map -a bs479.beagle.maf05.phy.vcf.gm -b superFST_sorted_arranged.bed -c 5,5,5,5,4 -o mean,min,max,median,collapse > fst_gwasSNP.txt

bedtools map -a bs479.beagle.maf05.phy.vcf.gm -b superFST_sorted2.bed -c 4,4,4,4,5 -o mean,min,max,median,collapse > fst_gwasSNP.txt


bedtools map -a bs479.beagle.maf05.phy.vcf.gm -b unique_superFST_sorted2.bed -c 4,4,4,4,5 -o mean,min,max,median,collapse > fst_gwasSNP_unique.txt # use this

#bedtools map -a bs479.beagle.maf05.phy.vcf.gm -b my.result.bed -c 5,4 -o mean,collapse -null " " > result.txt

head fst_gwasSNP.txt
echo "************************"
head fst_gwasSNP_unique.txt



# fst based on annotation
bsGffPhy="/dscrhome/jpm76/blastdb/lib/Boechera_phytozome/Bstricta/v1.2/annotation/Julius.Bstricta_278_v1.2.gene_exons.chr.gff3"

sed '1,2d' $bsGffPhy | paste <(cut -f 1,3,4) <(cut -f 2,5,6,7) > test.gff

sed '1d' $bsGffPhy | sed '1d' | grep CDS | cut -f 1, 

wc -l <(sed '1,2d' $bsGffPhy | cut -f 1,3,4 | head) 

wc -l <(sed '1,2d' $bsGffPhy | cut -f 2,5,6,7 | head) 

# sorts the gff

paste <(sed '1,2d' $bsGffPhy | cut -f 1,3,4) <(sed '1,2d' $bsGffPhy | cut -f 2,5,6,7) > bs.gff; sort -k1,1 -k2,2n bs.gff > $.tmp && mv $.tmp bs.gff; head bs.gff

# checks bedfile start-stop

head bs.gff; echo "*********";awk '{if ($2 > $3) {$6=$3; $7=$2} else {$6=$2 ; $7=$3} print $0}' OFS='\t' bs.gff > zdel; head zdel

head bs.gff; paste <(cut -f 1,6,7 zdel) <(cut -f 4-7 bs.gff) > bs.gff.bed; echo " ********************* "; head bs.gff.bed


# CDS only
cat bs.gff.bed | grep CDS > cds.only.bed

# exon only
cat bs.gff.bed | grep exon > exon.only.bed; head exon.only.bed

# gene only
cat bs.gff.bed | grep gene > gene.only.bed; head gene.only.bed

# exon1 only
paste <(cut -f 6 exon.only.bed | sed 's/\./\t/g' | awk '{if ($7==1) {$1="keep"} else {$1="discard"} print $0}' OFS='\t' | cut -f 1) <(cat exon.only.bed) | grep keep | cut -f 2-7 > exon1.only.bed; head exon1.only.bed


# make 10kb windows in the genome
bedtools makewindows -g Bs.genome.sizes_phy.txt -w 10000 > Bs.genome.10kbWindows.bed


# get only the max for FST within 10kb windows for each EPC
bedtools map -a Bs.genome.10kbWindows.bed -b unique_superFST_sorted2.bed -c 4,4,4 -o max,median,mean | sed '1 i chromosome\tstart\tstop\tmax_FST\tmedian_FST\tmean_FST' > max_median_meanFST.10kbwin.EPC.txt; head max_median_meanFST.10kbwin.EPC.txt


# 20kb windows
bedtools map -a Bs.genome.20kbWindows.bed -b unique_superFST_sorted2.bed -c 4,4,4 -o max,median,mean | sed '1 i chromosome\tstart\tstop\tmax_FST\tmedian_FST\tmean_FST' > max_median_meanFST.20kbwin.EPC.txt; head max_median_meanFST.20kbwin.EPC.txt


bedtools map -a Bs.genome.20kbWindows.bed -b mod.pc123.txt -c 4 -o min > minPval_pc123.20kbwin.EPC.txt; head minPval_pc123.20kbwin.EPC.txt


# ----------- Mon Feb 11 09:53:57 EST 2019

# pc123

awk '{print "chr"$4"\t"$5"\t"$5"\t"$3}' pc123.txt | sort -k1,1 -k2,2n  > mod.pc123.txt

bedtools map -a Bs.genome.20kbWindows.bed -b mod.pc123.txt -c 4 -o min -null ""  > minPval_pc123.20kbwin.EPC.txt; head minPval_pc123.20kbwin.EPC.txt

awk '{print $0"\t"""}' minPval_pc123.20kbwin.EPC.txt |  awk '{if ($4 <= 1e-4 && $4!="") {$5="hit"} else {$5="boring"} print}' OFS='\t' | grep -w "hit" > 1e_neg4_gwas.bed

awk '{print $0"\t"""}' minPval_pc123.20kbwin.EPC.txt |  awk '{if ($4 <= 1e-8 && $4!="") {$5="hit"} else {$5="boring"} print}' OFS='\t' | grep -w "hit" > 1e_neg8_gwas.bed


awk '{print $0"\t"""}' minPval_pc123.20kbwin.EPC.txt |  awk '{if ($4=="") {$5="delete"} else {$5="keep"} print}' OFS='\t' | grep -w "hit" > windows_w_pval.bed

grep -w "fst_outlier_99th_percentile" 20kbwindow_fst_99_995.bed |\
cut -f1,2,3 > 99th_percentile.meanfst	

grep -w "fst_outlier_995th_percentile" 20kbwindow_fst_99_995.bed |\
cut -f1,2,3 > 99.5th_percentile.meanfst	

bedtools intersect -a 1e_neg4_gwas.bed -b 99th_percentile.meanfst 

# bs479 markers
sed '1d' Bs475_beagle.MAF05.phy.vcf.gm | awk '{print $0"\t"$3}' | awk '{print "chr"$2"\t"$3"\t"$4"\t"$1}' > gwas.snps.bed

bedtools intersect -a 1e_neg4_gwas.bed -b gwas.snps.bed 
bedtools intersect -a 1e_neg8_gwas.bed -b gwas.snps.bed


bedtools intersect -a 1e_neg4_gwas.bed -b 99.5th_percentile.meanfst > neg4__99.5th_percentile.bed
bedtools intersect -a 1e_neg4_gwas.bed -b 99th_percentile.meanfst > neg4__99th_percentile.bed
bedtools intersect -a 1e_neg8_gwas.bed -b 99th_percentile.meanfst > neg8__99th_percentile.bed
bedtools intersect -a 1e_neg8_gwas.bed -b 99.5th_percentile.meanfst > neg8__99.5th_percentile.bed # empty

# significant SNPs in windows

bedtools intersect -a 1e_neg4_gwas.bed -b gwas.snps.bed

#snps in the outlier
bedtools intersect -a 99th_percentile.meanfst -b gwas.snps.bed 


#snps that are hits and outlier

bedtools intersect -a neg4__99.5th_percentile.bed -b gwas.snps.bed
bedtools intersect -a neg8__99th_percentile.bed -b gwas.snps.bed


# pipeline

#0. make appropriate window


#1.GWAS hits

output=gs.pc12.txt
f1=pc1.leafGS.forCMplot
f2=pc2.leafGS.forCMplot
f3=
f4=
f5=
cat $f1 $f2 $f3 $f4 $f5 | awk '{print "chr"$4"\t"$5"\t"$5"\t"$3}'  | sort -k1,1 -k2,2n  > $output; wc -l $output; head $output; echo "...."; tail $output

# map min pval at 20kb windows
. ./common.lib; mapbed file1.bed file2.bed



. ./common.lib; categorize gs.pc12.txt lessthan 1e-4 winner loser gs.pc12.categ.txt; head gs.pc12.categ.txt




#2. FST outliers



